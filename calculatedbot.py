import bs4
import urllib2
import requests
import praw
import pdb
import re
import os
from bs4 import BeautifulSoup
from lxml import html
import time


gameSite='http://www.espn.com/soccer/matchstats?gameId=498148'
f=open('/users/gavininglis/desktop/webdev/sixersbot/espnData.txt','w')
error=open('/users/gavininglis/desktop/webdev/sixersbot/errorEspn.txt','w')
soup=BeautifulSoup(urllib2.urlopen(gameSite),"lxml")

team1=soup.find('th',{'class' : 'home'}).getText().replace('\n','')
team2=soup.find('th',{'class' : 'away'}).getText().replace('\n','')
score1=soup.find('span',{'class' : 'score icon-font-after'}).getText()
score2=soup.find('span',{'class' : 'score icon-font-before'}).getText()
time=soup.find('span',{'class' : 'game-time'}).getText()
score1=int(score1) 
score2=int(score2)

reddit = praw.Reddit('bot1')
subreddit = reddit.subreddit('pythonforengineers') #fetch subreddit
post = subreddit.submit(team1+ ' vs. ' +team2,'')

response=team1+ ' ' +team2+'\n\n'+' ' + str(score1) + ' - '+ str(score2)+'\n\n'+'   '+ time
post.edit(response)


#Create the reddit instance
reddit = praw.Reddit('bot1')
#next two jawns store post and comment id in two files
if not os.path.isfile("posts_replied_to.txt"):
    posts_replied_to = []
    
else:
    with open("posts_replied_to.txt", "r") as f: 
       posts_replied_to = f.read()
       posts_replied_to = posts_replied_to.split("\n")
       posts_replied_to = list(filter(None, posts_replied_to))

if not os.path.isfile("replied_to.txt"):
    replied_to = []
    
else:
    with open("replied_to.txt", "r") as f:
       replied_to = f.read()
       replied_to = replied_to.split("\n")
       replied_to = list(filter(None, replied_to))

posts_replied_to = list(filter(None, posts_replied_to))
replied_to = list(filter(None, replied_to))

subreddit = reddit.subreddit('all') #fetch subreddit

comments = subreddit.stream.comments() #fetch comments

KEYWORDS = {
        # 'sorry!':'Calculated. \n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'savage!': 'Calculated. \n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'calculated.': "That's my line! Calculated. \n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*",
        # 'whoops': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'whoops!': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        # 'Thanks!': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'siiick': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        # 'trash': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'toxic': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'what a save!': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'what a save': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        # 'nice shot!': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        # 'nice shot': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',
        'Chat disabled': 'Calculated.\n\n_____\n\n ^*Chat* ^*disabled* ^*for* ^*3* ^*seconds.*',

        }   

for submission in subreddit.hot(limit=100):
    if submission.id in posts_replied_to:
        continue
    for item, response in KEYWORDS.items():
        search_in = unicode('{}{}').format(submission.title, submission.selftext) #added unicode to resolve error in terminal
        if re.match(item, search_in,re.IGNORECASE):
            submission.reply(response)
            print("Bot replying to : ", submission.title)
            posts_replied_to.append(submission.id)
            with open("posts_replied_to.txt", "w") as f:
                for post_id in posts_replied_to:
                    f.write(post_id + "\n")

for comment in comments: 
    submission.coment_sort = 'new'
    #for each comment in the comments stream. the current comment being processed is called "comment"
    text = comment.body 
    author = comment.author
    if author == "calculatedbot":
        continue
    if comment.id  in replied_to:  
        continue
    for item, response in KEYWORDS.items():
        search_in = unicode('{}').format(text) #unicode resolves terminal error
        if re.match(item,search_in,re.IGNORECASE):
            replied_to.append(comment.id)
            comment.reply(response)
            print("Bot replying to : ", author)
            with open("replied_to.txt", "w") as f:
                for post_id in posts_replied_to:
                    f.write(post_id + "\n")


time.sleep(60)